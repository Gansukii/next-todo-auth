import { createContext, FC, useContext, ReactNode, useReducer, useMemo } from "react";

export interface StateModifiers {
  openSidebar: () => void;
  closeSidebar: () => void;
}

export interface StateValues {
  //   user: {[key:string]: string};
  user: any;
  todos: any[];
}

// const stateModifiers = {
//   openSidebar: () => {},
//   closeSidebar: () => {},
// };

const initialState = { user: {}, todos: [] };

// type State = StateValues & StateModifiers;

const StateContext = createContext<StateValues>({
  ...initialState,
});

// type Action = { type: "OPEN_SIDEBAR" | "CLOSE_SIDEBAR" };

// function uiReducer(state: StateValues, action: Action) {
//   switch (action.type) {
//     case "OPEN_SIDEBAR": {
//       return {
//         ...state,
//         isSidebarOpen: true,
//       };
//     }
//     case "CLOSE_SIDEBAR": {
//       return {
//         ...state,
//         isSidebarOpen: false,
//       };
//     }
//   }
// }

interface ProviderProps extends StateValues {
  children: ReactNode | ReactNode[];
}

export const AppProvider: FC<{ children: ReactNode }> = ({ children }) => {
  //   const [state, dispatch] = useReducer(uiReducer, initialState);
  //   const openSidebar = () => dispatch({ type: "OPEN_SIDEBAR" });
  //   const closeSidebar = () => dispatch({ type: "CLOSE_SIDEBAR" });

  let value = { user: {}, todos: [] };

  //   const value = useMemo(() => {
  //     return {
  //       user,
  //       todo,
  //     };
  //   }, [user]);

  return <StateContext.Provider value={value}>{children}</StateContext.Provider>;
};

export const useStateContext = () => {
  const context = useContext(StateContext);
  return context;
};
