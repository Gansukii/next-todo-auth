import { FC, useState } from "react";
import styles from "./SignIn.module.css";
import { signInHandler } from "../../apiHandlers";
import { useRouter } from "next/router";

const SignIn: FC = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();

  const handleSignIn = async () => {
    setIsLoading(true);
    if (username.trim() === "" || password.trim() === "") {
      alert("Empty field!");
      return;
    }

    const data = await signInHandler({ username, password });

    // if (data.status === 200) {
    //   router.push("/todos");
    // } else if (data.status === 403) {
    //   alert("Invalid username or password");
    // } else {
    //   alert("an error occured");
    // }
    const userData = await data.json();

    if (userData.success) {
      router.push("/todos");
    } else {
      alert(data.message);
    }
    setIsLoading(false);
  };

  return (
    <>
      {/* <div className={styles.card}> */}
      <div className="w-full max-w-xs mx-auto mt-32">
        <form className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
          <div className="flex justify-center mb-5 text-black text-3xl font-bold tracking-wide">Sign In</div>

          <div className="mb-4">
            <label className="block text-gray-700 text-md font-bold mb-2" htmlFor="username">
              Username
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:outline-none focus:shadow-outline"
              id="username"
              type="text"
              placeholder="Username"
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>

          <div className="mb-4">
            <label className="block text-gray-700 text-md font-bold mb-2" htmlFor="password">
              Password
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight  focus:outline-none focus:shadow-outline"
              id="password"
              type="password"
              placeholder="Password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div className="flex items-center ">
            <button
              className="bg-blue-500 w-full hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              // type="button"
              disabled={isLoading}
              onClick={handleSignIn}
            >
              {isLoading ? "Signing in..." : "Sign In"}
            </button>
          </div>
        </form>
      </div>
    </>
  );
};

export default SignIn;
