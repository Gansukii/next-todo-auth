import { FC, useEffect, useState } from "react";
import { profileHandler } from "../../apiHandlers";

const Navbar: FC = () => {
  const [username, setUsername] = useState("");
  let user;

  useEffect(() => {
    const getUser = async () => {
      user = await profileHandler();
      if (user.success) {
        console.log(user.user);
        setUsername(user.user.username!);
      }
    };
    getUser().catch(() => alert("Error while fetching the data"));
  }, []);

  return (
    <div className="text-4xl font-bold px-5 py-3 shadow-sm shadow-slate-300 ">
      Next-Todo <div>welcome {username}</div>
    </div>
  );
};

export default Navbar;
