import { FC, useState } from "react";
import { addTodoHandler } from "../../apiHandlers/todos";
import { useRouter } from "next/router";
// import { useStateContext } from "../context/context";

type stateType = { [key: string]: any };
type inputType = {
  state: stateType[];
  setState: any;
};

const TodoInput: FC<inputType> = ({ state, setState }) => {
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const router = useRouter();
  // let { todos } = useStateContext();

  const handleTodoAdd = async () => {
    setIsLoading(true);
    if (title.trim() === "" || description.trim() === "") {
      alert("Missing field/s");
      setIsLoading(false);
      return;
    }
    const data = await addTodoHandler({ title, description });

    if (data.todo) {
      let newState = [data.todo, ...state];
      setState(newState);
    } else {
      alert("User Unauthenticated");
      router.push("/");
    }

    setIsLoading(false);
  };
  return (
    <>
      <div className="col-start-3 col-span-4 shadow-sm border border-slate-200  px-5 pt-4 bg-slate-50">
        <div className="text-3xl font-semibold mb-5">Add new To Do:</div>
        <div className="md:flex md:items-center mb-6">
          <div className="md:w-1/6">
            <label className=" text-gray-800 text-lg font-semibold  mb-1 md:mb-0 pr-4" htmlFor="title">
              Title:
            </label>
          </div>
          <div className="md:w-5/6">
            <input
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              name="title"
              placeholder="title"
              type="text"
              value={title}
              onChange={(e) => {
                setTitle(e.target.value);
              }}
            />
          </div>
        </div>
        <div className="md:flex md:items-center mb-6">
          <div className="md:w-1/6">
            <label className=" text-gray-800 text-lg font-semibold  mb-1 md:mb-0 pr-4" htmlFor="description">
              Description:
            </label>
          </div>
          <div className="md:w-5/6">
            <input
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              name="description"
              placeholder="description"
              type="text"
              value={description}
              onChange={(e) => {
                setDescription(e.target.value);
              }}
            />
          </div>
        </div>

        <div className="flex items-center ">
          <button
            className="bg-blue-500 w-full text-lg font-semibold mb-5 hover:bg-blue-700 text-white py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            // type="button"
            disabled={isLoading}
            onClick={handleTodoAdd}
          >
            Add To Do
          </button>
        </div>
      </div>
    </>
  );
};

export default TodoInput;
