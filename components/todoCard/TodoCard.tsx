import { FC, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFloppyDisk, faTrash } from "@fortawesome/free-solid-svg-icons";
import { deleteTodoHandler, updateTodoHandler } from "../../apiHandlers/todos";

type stateType = { [key: string]: any };
type inputType = {
  title: string;
  description: string;
  id: string;
  state: stateType[];
  setState: any;
};

const TodosCard: FC<inputType> = (input: inputType) => {
  const { title, description, id, state, setState } = input;
  const [todoTitle, setTodoTitle] = useState(title);
  const [todoDescription, setTodoDescription] = useState(description);
  const [isUpdating, setIsUpdating] = useState(false);

  const handleDelete = async (): Promise<void> => {
    const data = await deleteTodoHandler({ id });
    if (data.success) {
      let newState = state.filter((todo) => todo._id !== id);
      setState(newState);
    }
  };

  const handleUpdate = async () => {
    const data = await updateTodoHandler({ title: todoTitle, description: todoDescription, id });
    if (data.success) {
      setIsUpdating(false);
      // let updatedIndex = state.findIndex((todo) => todo._id === id);
      let newState = state.map((todo) => {
        if (todo._id === id) {
          todo.title = todoTitle;
          todo.description = todoDescription;
        }

        return todo;
      });

      setState(newState);
    }
  };

  return (
    <>
      <div className="col-start-3 col-span-4 shadow-md flex px-5 py-3 bg-slate-100">
        <div className=" w-5/6">
          {isUpdating ? (
            <>
              <input
                type="text"
                className="text-2xl font-semibold block w-full mb-2"
                value={todoTitle}
                onChange={(e) => setTodoTitle(e.target.value)}
              />
              <textarea
                // type="textarea"
                className="text-lg block w-full mb-2 "
                value={todoDescription}
                onChange={(e) => setTodoDescription(e.target.value)}
              />
            </>
          ) : (
            <>
              <div className="text-2xl font-semibold " onClick={() => setIsUpdating(true)}>
                {title}
              </div>
              <div className="text-lg  max-h-20 overflow-y-auto flex-wrap" onClick={() => setIsUpdating(true)}>
                {description}
              </div>
            </>
          )}

          {/* <div className="text-lg  ">{description}</div> */}
        </div>
        <div className="w-1/6 text-3xl flex items-center ">
          {isUpdating && (
            <FontAwesomeIcon className="ml-auto cursor-pointer" icon={faFloppyDisk} onClick={() => handleUpdate()} />
          )}
          <FontAwesomeIcon className="ml-auto cursor-pointer text-red-500" icon={faTrash} onClick={() => handleDelete()} />
        </div>
      </div>
    </>
  );
};

export default TodosCard;
