import { NextApiRequest, NextApiResponse } from "next";
import { resolve } from "path";

type addToDoInput = {
  title: string;
  description: string;
  id?: string;
};

type deleteInput = {
  id: string;
};

type ReturnType = {
  todo: [{ [key: string]: any }];
};

export const addTodoHandler = async (input: addToDoInput): Promise<ReturnType> => {
  const data = await fetch("/api/todos", {
    method: "POST",
    body: JSON.stringify({ ...input }),
    headers: {
      "Content-type": "application/json",
    },
  });

  return data.json();
};
export const getTodoHandler = async (): Promise<any> => {
  const data = await fetch("http://localhost:3000/api/todos", {
    method: "GET",
    headers: {
      "Content-type": "application/json",
    },
  });

  return await data.json();
};

export const deleteTodoHandler = async (input: deleteInput): Promise<any> => {
  const data = await fetch("/api/todos", {
    method: "DELETE",
    body: JSON.stringify({ ...input }),
    headers: {
      "Content-type": "application/json",
    },
  });

  return data.json();
};

export const updateTodoHandler = async (input: addToDoInput): Promise<any> => {
  const data = await fetch("/api/todos", {
    method: "PUT",
    body: JSON.stringify({ ...input }),
    headers: {
      "Content-type": "application/json",
    },
  });

  return data.json();
};
