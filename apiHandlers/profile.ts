import { NextPageContext } from "next";

export default async function (): Promise<any> {
  const user = await fetch("http://localhost:3000/api/profile", {
    method: "GET",
    headers: {
      "Content-type": "application/json",
    },
  });

  return user.json();
}
