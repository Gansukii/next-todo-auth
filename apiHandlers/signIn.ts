interface signInInputType {
  username: string;
  password: string;
}

export default async function (input: signInInputType): Promise<any> {
  const data = await fetch("/api/signIn", {
    method: "POST",
    body: JSON.stringify({ ...input }),
    headers: {
      "Content-type": "application/json",
    },
  });

  // if(data.status === 200){

  // }
  return data;
}
