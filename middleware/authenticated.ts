import { NextApiHandler, NextApiRequest, NextApiResponse } from "next";
import { JwtPayload, verify, VerifyErrors } from "jsonwebtoken";

const config = require("config");

export const authenticated = (fn: NextApiHandler) => async (req: NextApiRequest, res: NextApiResponse) => {
  verify(
    req.cookies.auth!,
    config.JWT_SECRET,
    async function (err: VerifyErrors | null, decoded: string | JwtPayload | undefined) {
      if (!err && decoded) {
        return await fn(req, res);
      }
      res.status(401).json({ message: "user unauthenticated!", success: false });
    }
  );
};
