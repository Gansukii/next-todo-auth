import mongoose from "mongoose";
import { Schema, model, Mongoose } from "mongoose";

interface ITodo {
  userId: string;
  title: string;
  description: string;
  createdAt: Date;
}

const todoSchema = new Schema<ITodo>({
  userId: { type: String, required: true },
  title: { type: String, required: true },
  description: { type: String, required: true },
  createdAt: { type: Date, default: new Date() },
});

// export const Todo = model<ITodo>("Todo", todoSchema);
export const Todo = mongoose.models.Todo || mongoose.model("Todo", todoSchema);
