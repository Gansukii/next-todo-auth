import mongoose from "mongoose";
import { Schema, model, Mongoose } from "mongoose";

interface IUser {
  username: string;
  password: string;
}

const userSchema = new Schema<IUser>({
  username: { type: String, required: true },
  password: { type: String, required: true },
});

// export const User = model<IUser>("User", userSchema);
export const User = mongoose.models.User || mongoose.model("User", userSchema);
