import type { NextPage, NextPageContext } from "next";
import { Layout, TodoCard, TodoInput } from "../components";
import { authInitialProps } from "../utils/authInitialProps";
import { GetStaticPaths, GetStaticPropsContext, InferGetStaticPropsType } from "next";
import { getTodoHandler } from "../apiHandlers/todos";
import { useStateContext } from "../components/context/context";
import { useEffect, useState } from "react";
import { profileHandler } from "../apiHandlers";
import Cookies from "js-cookie";
import { sign, verify } from "jsonwebtoken";
import { useRouter } from "next/router";

// export async function getStaticProps() {
//   const user = await profileHandler();

//   const todo = await getTodoHandler();

//   return {
//     props: {
//       todo,
//     },
//     revalidate: 4 * 60 * 60,
//   };
// }

type stateType = { [key: string]: any };
interface returnType {
  message: { [key: string]: string };
  success: boolean;
  todo?: stateType[];
}
// const Todos = ({ todo }: InferGetStaticPropsType<typeof getStaticProps>) => {
const Todos = () => {
  let arr: stateType[] = [];
  const [todosState, setTodosState] = useState(arr);
  const router = useRouter();
  let todo: returnType;

  useEffect(() => {
    const geTodo = async () => {
      todo = await getTodoHandler();
      if (todo.success) {
        setTodosState(todo.todo!);
      } else {
        alert(todo.message);
        router.push("/");
      }
    };

    // setTodosState(Object.keys(todo.todo).length === 0 ? [] : todo.todo);
    geTodo().catch(() => alert("Error while fetching the data"));
  }, []);

  // useEffect(() => {
  //   console.log(todo);
  // }, [todo]);

  useEffect(() => {}, [todosState]);

  return (
    <>
      <div className="px-10 grid grid-cols-8 gap-4 justify-center mt-11 pb-10">
        <TodoInput state={todosState} setState={setTodosState} />
        {todosState.map((todoItem) => (
          <TodoCard
            key={todoItem._id}
            id={todoItem._id.toString()}
            title={todoItem.title}
            description={todoItem.description}
            state={todosState}
            setState={setTodosState}
          />
        ))}
      </div>
    </>
  );
};

// Todos.getInitialProps = authInitialProps(true);

Todos.Layout = Layout;

export default Todos;
