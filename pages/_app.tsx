// import "../styles/globals.css";
import "../styles/main.css";
import type { AppProps } from "next/app";
import { FC, ReactNode } from "react";
// import { AppProvider } from "../components/context/Context";
// import { useStateContext } from "../components/context/Context";
// import { SessionProvider } from "next-auth/react";

const Noop: FC<{ children: ReactNode }> = ({ children }) => <>{children}</>;

function MyApp({ Component, pageProps }: AppProps & { Component: { Layout: FC<{ children: ReactNode }> } }) {
  const Layout = Component.Layout ?? Noop;

  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
}

export default MyApp;
