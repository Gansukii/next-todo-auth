import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import { Layout, SignIn } from "../components";

// export default function Home() {
const Home = () => {
  return (
    <>
      <div className="flex items-center">
        <SignIn />
      </div>
    </>
  );
};

Home.Layout = Layout;

export default Home;
