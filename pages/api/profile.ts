import { NextApiHandler, NextApiRequest, NextApiResponse } from "next";
import dbConnect from "../../utils/dbConnect";
import { Todo } from "../../models/TodoModel";
import { User } from "../../models/UserModel";
import { Schema, model } from "mongoose";
import { json } from "stream/consumers";
import { sign, verify } from "jsonwebtoken";
import { authenticated } from "../../middleware/authenticated";
import { resolve } from "path";

const config = require("config");

dbConnect();

export default authenticated(async (req: NextApiRequest, res: NextApiResponse) => {
  const { cookies } = req;
  var decoded: any = verify(cookies.auth, config.JWT_SECRET);
  let user = await User.findOne({ _id: decoded.userObj.id });
  // let user = await User.find();

  if (!user) {
    return res.status(404).json({ user: {} });
  }

  return res.status(200).json({ user, success: true });
});
