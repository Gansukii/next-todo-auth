import { NextApiRequest, NextApiResponse } from "next";
import dbConnect from "../../utils/dbConnect";
import { User } from "../../models/UserModel";
import { Schema, model } from "mongoose";
import { sign } from "jsonwebtoken";
import cookie from "cookie";

dbConnect();

export default async (req: NextApiRequest, res: NextApiResponse) => {
  switch (req.method) {
    case "GET":
      // const user = await User.find(req.body);
      const userz = await User.find(req.body);
      if (!userz) {
        return res.status(404).json({ success: false, user: {} });
      }

      return res.status(200).json({ success: true, user: userz });
      break;

    case "POST":
      // const newUser = new User(req.body);
      // const data = await newUser.save();

      const user = await User.find(req.body);
      if (user.length < 1) {
        return res.status(403).json({ message: "user does not exist" });
      }

      const userObj = {
        id: user[0]._id,
        name: user[0].username,
      };

      const jwt = sign({ userObj }, "2634622b-02de-4e41-bce0-fdf55620c90e", { expiresIn: 60 * 60 });

      res.setHeader(
        "Set-Cookie",
        cookie.serialize("auth", jwt, {
          httpOnly: true,
          secure: process.env.NODE_ENV !== "development",
          sameSite: "strict",
          maxAge: 3600,
          path: "/",
        })
      );

      // res.status(200).json({ aughtToken: jwt });
      res.status(200).json({ success: true, status: 200, user: userObj });
      break;

    default:
      return res.status(404).json({ user: {} });
      break;
  }

  //   const user = new User(req.body);

  //   const data = await user.save();

  //   console.log(data);
};
