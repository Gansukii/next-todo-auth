import { NextApiHandler, NextApiRequest, NextApiResponse } from "next";
import dbConnect from "../../utils/dbConnect";
import { Todo } from "../../models/TodoModel";
import { Schema, model } from "mongoose";
import { json } from "stream/consumers";
import { verify } from "jsonwebtoken";
import { authenticated } from "../../middleware/authenticated";
import { resolve } from "path";

const config = require("config");
dbConnect();

//TODO ADD AUTHENTICATED-------
export default authenticated(async (req: NextApiRequest, res: NextApiResponse) => {
  switch (req.method) {
    case "GET":
      try {
        const cookies = req.cookies;
        // if (Object.keys(cookies).length === 0) {
        //   console.log("unauth");
        //   return res.status(200).json(todo);
        // }
        const decoded: any = verify(cookies.auth, config.JWT_SECRET);

        // let todo = await Todo.find({});
        let todo = await Todo.find({ userId: decoded.userObj.id });

        if (!todo) {
          return res.status(404).json({ todo: {} });
        }

        todo = todo.reverse();
        return res.status(200).json({ todo, success: true });
      } catch {
        return res.status(404).json({ todo: {} });
      }
    case "POST":
      try {
        const { cookies } = req;
        const decoded: any = verify(cookies.auth, config.JWT_SECRET);

        const todoObj = req.body;
        todoObj["userId"] = decoded.userObj.id;

        const newTodo = new Todo(req.body);
        // console.log(newTodo);
        const todo = await newTodo.save();

        // console.log(todo);

        return res.status(200).json(todo);
      } catch {
        return res.status(500).json({ message: "User Unauthenticated" });
      }
    case "DELETE":
      try {
        const deleted = await Todo.deleteOne({ _id: req.body.id });
        return res.status(200).json({ message: "Todo entry deleted!", success: true });
      } catch {
        return res.status(500).json({ message: "an error occured", success: true });
      }
    case "PUT":
      try {
        const updated = await Todo.updateOne(
          { _id: req.body.id },
          { $set: { title: req.body.title, description: req.body.description } }
        );
        return res.status(200).json({ message: "Todo entry updated!", success: true });
      } catch {
        return res.status(500).json({ message: "an error occured", success: false });
      }
    default:
      return res.status(404).json({ todo: {} });
  }

  resolve();
});

//TOdo api warning on get  https://stackoverflow.com/questions/60684227/api-resolved-without-sending-a-response-in-nextjs
