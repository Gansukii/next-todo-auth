import mongoose from "mongoose";

const config = require("config");

const connection = {};

async function dbConnect() {
  if (connection.isConnected) return;

  const db = await mongoose.connect(config.get("MONGO_URI"), {
    useNewUrlParser: true,
    // useFindAndModify: false,
    useUnifiedTopology: true,
  });

  console.log("Database is connected");
  connection.isConnected = db.connections[0].readyState;
}

export default dbConnect;
