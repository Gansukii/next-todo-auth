import Router from "next/router";
import { JwtPayload, verify, VerifyErrors } from "jsonwebtoken";
import { NextApiHandler, NextApiRequest, NextApiResponse } from "next";
import { NextResponse, NextRequest } from "next/server";
import cookie from "cookie";

export const authInitialProps =
  (isProtected: boolean) =>
  ({ req, res }: { req: NextApiRequest; res: NextApiResponse }) => {
    // const data = "qwer";
    if (!req) return false;
    const cookies = cookie.parse(req.headers.cookie || "");

    const authKey = cookies.auth;

    verify(
      req.cookies.auth!,
      "2634622b-02de-4e41-bce0-fdf55620c90e",
      async function (err: VerifyErrors | null, decoded: string | JwtPayload | undefined) {
        if (err) {
          console.log("err");
        } else {
          console.log("goz");
        }
      }
    );

    return true;
  };
